//#if WANDER_VR

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZAnnotateInVolume: MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public InputActionProperty placePointKey;
        public InputActionProperty confirmKey;
        public InputActionProperty nextClassification;
        public GameObject placeVolumePointPrefab;
        public Material [] classifierMats;
        public Material volumeMaterial;

        [Header("LIDARQuery")]
        public int numPointsToProgress = 1;
        public float radius = 0.01f;
        public float distance = 3;

        [Header("Debug")]
        public GameObject debugInHullPoint;
        List<Vector3> lastHullPoints;
        List<int> lastHullTris;

        LineRenderer lineRenderer;
        List<GameObject> points;
        GK.ConvexHullCalculator calculator;
        XRWarningMessage warningMsg;

        // Query
        TraceLineQuery lineQuery;
        Vector3 foundPoint;
        bool lastValid;
        bool placedSinceValid;

        // Hull Query
        TraceHullQuery hullQuery;

        private void Awake()
        {
            if (loader == null )
                loader = FindObjectOfType<LAZLoader>();
            warningMsg = FindObjectOfType<XRWarningMessage>( true );
            lineRenderer = GetComponent<LineRenderer>();
            points = new List<GameObject>();
            calculator = new GK.ConvexHullCalculator();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
        }

        private void OnDisable()
        {
            points.ForEach( p => p.Destroy() );
            points.Clear();
            lineQuery = null;
            hullQuery = null;
            lastValid = false;
        }

        void Update()
        {
            if (hullQuery!=null)
            {
                AwaitHullQueryAndProcess();
            }
            else
            {
                PlacePointsAndStartHullQuery();
            }
            UpdateInput();
            DrawBeam();
        }

        private void AwaitHullQueryAndProcess()
        {
            if (hullQuery.done)
            {
                loader.StartGenericTask( () =>
                {
                    var hqSliced = hullQuery;
                    if (hqSliced == null) // When OnDisable is called in mean time, this might have become null (this is not executed on main thread).
                        return;

                    HashSet<LAZOctreeCell> uniqueCells = new HashSet<LAZOctreeCell>();

                    // Now we have the indices to the vertices in the cell.
                    for (int i = 0;i < hqSliced.indices.Count;i++)
                    {
                        var renderer = hqSliced.indices[i].Item1;
                        var lazCell  = hqSliced.indices[i].Item2;
                        var vertexId = hqSliced.indices[i].Item3;

                        uniqueCells.Add( lazCell );

                        // TODO, one issue here: if we apply a change to the cell, it will apply for all renderers pointing to this data.
                        lazCell.ApplyChangeToVertices( ( List<LAZVertex> vertices ) =>
                        {
                            LAZVertex vertex = vertices[vertexId];
                            vertex.classification = (ushort)(XRLAZState.activeClassification<<8); // 65535 is scaled to [0-1]. So pre-multiply with 256.
                            vertices[vertexId] = vertex;
                        } );
                    }

                    foreach (var cell in uniqueCells)
                    {
                        if (cell.LeafReady)
                        {
                            cell.WriteLeafData();
                            cell.MarkDirty();       // This causes it to reload even though it may not have been unloaded by the loader due to culling.
                        }
                    }

                    hullQuery = null;
                } );
            }
        }

        private void PlacePointsAndStartHullQuery()
        {
            // Start a line query to see which point of the lidar we are hitting.
            if (lineQuery == null)
            {
                Vector3 from = transform.position + transform.forward*lineRenderer.GetPosition(0).z;
                lineQuery = loader.StartLineTrace( from, transform.forward, numPointsToProgress, radius, distance );
            }
            else if (lineQuery != null && lineQuery.done) // Handle the result of a previously started ray query.
            {
                lastValid  = lineQuery.hit;
                foundPoint = lineQuery.vertex.position;
                placedSinceValid = false;
                lineQuery = null;
            }

            // If place point key is pressed, place a point.
            if (placePointKey.action.WasPressedThisFrame() && lastValid && !placedSinceValid)
            {
                placedSinceValid = true;
                var point = Instantiate( placeVolumePointPrefab, foundPoint, Quaternion.identity );
                points.Add( point );
            }

            // If done with placing points and confirm is pressed.
            if (points.Count == 6)
            {
                //if (points.Count < 4)
                //{
                //    warningMsg.AddWarning( "A minimum of 4 points is required." );
                //}
                //else
                {
                    // Build convex hull geometry
                    List<Vector3> verts = new List<Vector3>();
                    List<int> tris = new List<int>();
                    List<Vector3> normals = new List<Vector3>();
                    List<Vector3> hullPoints = points.Select( p => p.transform.position ).ToList();
                    calculator.GenerateHull( hullPoints, false, ref verts, ref tris, ref normals );

#if UNITY_EDITOR
                    lastHullPoints = verts;
                    lastHullTris = tris;
#endif


                    // Create mesh
                    Mesh m = new Mesh();
                    m.SetVertices( verts );
                    m.SetTriangles( tris, 0 );
                    m.SetNormals( normals );

                    // Add geometry to game object
                    GameObject volume = new GameObject("Volume");
                    var meshRenderer = volume.AddComponent<MeshRenderer>();
                    var meshFilter   = volume.AddComponent<MeshFilter>();
                    meshRenderer.sharedMaterial = volumeMaterial;
                    meshFilter.sharedMesh = m;

                    // Get points inside hull and mark them. Hull query is executed as task in pool.
                    hullQuery = loader.StartHullQuery( verts, tris );

                    // Remove points already on main thread.
                    points.ForEach( p => p.Destroy() );
                    points.Clear();

                    helperText.Grip = "Change classification";
                }
            }
        }

        private void UpdateInput()
        {
           // if (points.Count == 0)
            {
                if (nextClassification.action.WasPressedThisFrame())
                {
                    XRLAZState.activeClassification += 1;
                    if (XRLAZState.activeClassification == classifierMats.Length)
                    {
                        XRLAZState.activeClassification = 0;
                    }
                }
            }
        }

        private void DrawBeam()
        {
            float d = 0;
            if (lastValid)
            {
                d = (transform.position - foundPoint).magnitude;
            }
            else
            {
                d = distance;
            }
            lineRenderer.sharedMaterial = classifierMats[XRLAZState.activeClassification];
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, d ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = d;
            XRNetworkState.LaserColor[(int)XRHand.Left]  = lineRenderer.sharedMaterial.GetColor( "_Color" );
        }
    }
}

//#endif