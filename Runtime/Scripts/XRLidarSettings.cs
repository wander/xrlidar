using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Wander
{
    // DO NOT use OnEnable/Disable instead because there is no way in Update to detect if key is pressed then.
    [RequireComponent(typeof(Canvas))]
    public class XRLidarSettings : MonoBehaviour
    {
        [Header("Settings")]
        public Camera hmdCamera;
        public Button uiCloseButton;
        public InputActionProperty showHideKey;
        public HelperTexts helperTextsLeft;
        public HelperTexts helperTextsRight;
        public XRControllerHelperTexts helperTextLeft;
        public XRControllerHelperTexts helperTextRight;
        public float vrOffsetZ = 4;
        public float vrOffsetY = 0;
        public float angleOffset = 0;
        public float outOfRangeDistance = 5;
        public bool enableInitially = false;

        bool wasEnabled;

        private void Awake()
        {
            if (hmdCamera == null)
                hmdCamera = GetComponent<Canvas>().worldCamera;

            if ( uiCloseButton != null )
            {
                uiCloseButton.onClick.AddListener( () =>
                {
                    TurnOnOffUIInternal( false );
                } );
            }
        }

        private void Start()
        {
            TurnOnOffUIInternal( enableInitially );
        }

        void TurnOnOffUIInternal( bool enable )
        {
            for (int i = 0; i < transform.childCount; i++ )
            {
                transform.GetChild( i ).gameObject.SetActive( enable );
            }
            XRUIState.SettingsMenuOn = enable;
            if (enable)
            {
                RepositionVR();
                helperTextLeft?.SetHelperTexts( helperTextsLeft );
                helperTextRight?.SetHelperTexts( helperTextsRight );
                XRUIState.CallOnSettingsEnabled();
            }
            else
            {
                XRUIState.CallOnSettingsDisabled();
            }
        }

        private void Update()
        {
            if (showHideKey.action != null && showHideKey.action.WasPerformedThisFrame())
            {
                TurnOnOffUIInternal( true );
            }
        }

        void RepositionVR()
        {
            var cam = hmdCamera;
            var forward =  cam.transform.forward;
            forward.y = 0;
            forward.Normalize();
            forward = Quaternion.Euler( 0, angleOffset, 0 ) * forward;
            transform.position = cam.transform.position +  forward * vrOffsetZ + Vector3.up * vrOffsetY;
            transform.rotation = Quaternion.Euler( 0, cam.transform.eulerAngles.y+angleOffset, 0 );
        }


        // ---- Thes functions are invoked from clicking on a menu item in the XR Settings menu -----

        public void ClosetSettingsMenu() // calle
        {
            TurnOnOffUIInternal( false );
        }

        public void SetGlobalLODBias( float value )
        {
            int bias = Mathf.RoundToInt(value);
            var renderers = FindObjectsOfType<LAZRenderer>( true );
            for (int i = 0;i < renderers.Length;i++)
            {
                renderers[i].LODBias = bias;
            }
        }

        public void SetViewType( int value )
        {
            ViewType viewType = (ViewType)value;
            var renderers = FindObjectsOfType<LAZRenderer>( true );
            for (int i = 0;i < renderers.Length;i++)
            {
                renderers[i].viewType = viewType;
            }
        }

        public void SetQualityLevel( float value )
        {
            int quality = Mathf.RoundToInt( value );
            QualitySettings.SetQualityLevel( quality, true );
        }

        public void EnableDisablePostProcessing( bool enable )
        {
            var globalVolume = FindObjectOfType<Volume>( true );
            globalVolume.gameObject.SetActive( enable );
        }

    }
}

//#endif