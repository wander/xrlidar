using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZCirfumference : MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public InputActionProperty placePointKey;
        public InputActionProperty confirmKey;
        public GameObject placeCircumPrefab;
        public GameObject circumInfoPrefab;
        public Material validBeam;
        public Material invalidBeam;
        public Material circumLineMat;
        public float lineWidth = 0.01f;

        [Header("LIDARQuery")]
        public int numPointsToSkip = 50;
        public float radius = 0.1f;
        public float distance = 30;

        [Header("Debug")]
        public bool useRays = false;

        GameObject activeCircumGo;
        LineRenderer lineRenderer;
        List<Vector3> points;
        XRWarningMessage warningMsg;

        // Query
        TraceLineQuery query;
        Vector3 foundPoint;
        bool lastValid;
        bool placedSinceValid;


        private void Awake()
        {
            if (loader == null)
            {
                loader = FindObjectOfType<LAZLoader>();
            }
            warningMsg = FindObjectOfType<XRWarningMessage>( true );
            lineRenderer = GetComponent<LineRenderer>();
            points = new List<Vector3>();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
        }

        private void OnDisable()
        {
            activeCircumGo.Destroy();
            Cleanup();
        }

        void Cleanup()
        {
            activeCircumGo = null;
            points.Clear();
            query = null;
            lastValid = false;
        }

        void Update()
        {
            if (query == null)
            {
                Vector3 from = transform.position + transform.forward*lineRenderer.GetPosition(0).z;
                query = loader.StartLineTrace( from, transform.forward, numPointsToSkip, radius, distance );
            }
            else if (query != null && query.done)
            {
                lastValid  = query.hit;
                foundPoint = query.vertex.position;
                placedSinceValid = false;
                query = null;
            }

            if (placePointKey.action.WasPressedThisFrame() && lastValid && !placedSinceValid)
            {
                if ( activeCircumGo == null )
                {
                    activeCircumGo = new GameObject( "Circumference" );
                    var cr = activeCircumGo.AddComponent<LineRenderer>();
                    cr.sharedMaterial = circumLineMat;
                    cr.startWidth = cr.endWidth = lineWidth;
                }
                if (points.Count == 0)
                {
                    points.Add( foundPoint );
                }
                else
                {
                    points[points.Count-1] = foundPoint;
                }
                points.Add( transform.position + transform.forward * distance );
                var lr = activeCircumGo.GetComponent<LineRenderer>();
                lr.positionCount = points.Count;
                lr.SetPositions( points.ToArray() );
                placedSinceValid = true;
            }

            // Update position of beam to current last point
            if ( activeCircumGo != null )
            {
                var cr = activeCircumGo.GetComponent<LineRenderer>();
                if (lastValid)
                {
                    cr.SetPosition( cr.positionCount-1, foundPoint );
                }
                else
                {
                    cr.SetPosition( cr.positionCount-1, transform.position + transform.forward * distance );
                }
            }

            if (confirmKey.action.WasPressedThisFrame())
            {
                if (points.Count-1 /*subtract point to end of beam*/ < 3)
                {
                    warningMsg.AddWarning( "A minimum of 3 points is required." );
                }
                else
                {
                    // close the circumference
                    var cr = activeCircumGo.GetComponent<LineRenderer>();
                    cr.SetPosition( cr.positionCount-1, points[0] ); // replace last point which is the beam end with first to close the circumference.
                    activeCircumGo = null;
                    // calc circumference
                    float circumference = 0;
                    for ( int i = 0; i < points.Count-1; i++) // last point is duplicate of first
                    {
                        var point  = points[i];
                        var point2 = points[i+1];
                        circumference += Vector3.Distance( point, point2 );
                    }
                    // find closest point to camera on circumference
                    float closestDist = float.PositiveInfinity;
                    int vClosestIdx = 0;
                    for ( int i = 0; i < points.Count; i++ )
                    {
                        float projDist = Vector3.Dot( points[i] - Camera.main.transform.position, Camera.main.transform.forward );
                        if ( projDist < closestDist )
                        {
                            vClosestIdx = i;
                            closestDist = projDist;
                        }
                    }
                    GameObject circumInfo = Instantiate(circumInfoPrefab);
                    //  volumeInfo.transform.parent = volume.transform;
                    circumInfo.transform.position = points[vClosestIdx];
                    circumInfo.GetComponentInChildren<TextMeshPro>().text = $"c={circumference.ToString("0.0")}m";
                    // set rotation
                    var f = Camera.main.transform.forward;
                    f.y = 0;
                    f.Normalize();
                    circumInfo.transform.forward = f;
                    // clean up
                    Cleanup();
                }
            }
            
            DrawBeam();
        }

        private void DrawBeam()
        {
            float d;
            if (lastValid)
            {
                d = (transform.position - foundPoint).magnitude;
            }
            else
            {
                d = distance;
            }
            lineRenderer.sharedMaterial = lastValid ? validBeam : invalidBeam;
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, d ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = d;
            XRNetworkState.LaserColor[(int)XRHand.Left]  = lastValid ? validBeam.GetColor( "_Color" ) : invalidBeam.GetColor( "_Color" );
        }
    }
}
