using UnityEngine;


namespace Wander
{
    /* Script to auto scale to cam with min and max distance. */
    public class ScaleToCam : MonoBehaviour
    {
        public float minDistance = 0.1f;
        public float maxDistance = 30;
        public float scalePerMtr = 0.1f;

        Vector3 originalScale;
        private void Start()
        {
            originalScale = transform.localScale;
        }

        private void Update()
        {
            float dist = (Camera.main.transform.position - transform.position).magnitude;
            float scale = Mathf.Clamp(dist-minDistance, minDistance, maxDistance) * scalePerMtr;
            transform.localScale = originalScale*(scale);
        }
    }
}