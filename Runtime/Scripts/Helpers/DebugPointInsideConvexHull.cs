using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Wander
{
    [ExecuteAlways()]
    public class DebugPointInsideConvexHull : MonoBehaviour
    {
        public GameObject testPoint;
        public bool checkIfBoundingBoxIsInside;

        public void Start()
        {
        }

        public void Update()
        {
            if (testPoint == null)
                return;

            var f = GetComponent<MeshFilter>();
            var r = testPoint.GetComponent<MeshRenderer>();
            var verts = f.sharedMesh.vertices.ToList();
            for ( int i = 0; i < verts.Count; i++)
            {
                verts[i] = transform.TransformPoint( verts[i] );
            }
            var tris = f.sharedMesh.triangles.ToList();
            if ((checkIfBoundingBoxIsInside && GeomUtil.IsBoundingBoxInsideConvexHull(verts, r.bounds)) || 
                (!checkIfBoundingBoxIsInside && GeomUtil.IsPointInsideConvexHull( testPoint.transform.position, verts, tris ) ))
            {
                testPoint.transform.localScale = Vector3.one*4;
            }
            else
            {
                testPoint.transform.localScale = Vector3.one;
            }
        }
    }
}