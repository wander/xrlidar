using UnityEngine;


namespace Wander
{
    /* Script to always face camera. */
    public class AutoFaceCam : MonoBehaviour
    {
        public bool zeroY = true;
        public float minDist = 3;

        private void Update()
        {
            var dir = Camera.main.transform.forward;
            if ( zeroY )
            {
                dir.y = 0;
                dir.Normalize();
            }

            if (Vector3.Distance( Camera.main.transform.position, transform.position ) > minDist)
            {
                transform.rotation = Quaternion.LookRotation( dir );
            }
        }
    }
}