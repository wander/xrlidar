//#if WANDER_VR

using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZRemoveObjects: MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public InputActionProperty activateKey;
        public Material beamMat;
        public float distance = 100;
        public float lineWidthHighlighted = 0.04f;

        LineRenderer lineRenderer;
        float prevLineWidth;
        GameObject lastHit;


        private void Awake()
        {
            if (loader == null )
                loader = FindObjectOfType<LAZLoader>();
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void OnEnable()
        {
            prevLineWidth = lineRenderer.startWidth;
            lineRenderer.sharedMaterial = beamMat;
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
            XRNetworkState.LaserColor[(int)XRHand.Left] = beamMat.GetColor( "_Color" );
        }

        private void OnDisable()
        {
            // Reinstate old line width
            lineRenderer.startWidth = lineRenderer.endWidth = prevLineWidth;
        }

        void Update()
        {
            lastHit = null;
            Vector3 from = transform.position + transform.forward*lineRenderer.GetPosition(0).z;
            if (Physics.Raycast( from, transform.forward, out RaycastHit hit, distance, LayerMask.GetMask( "Target" ) ))
            {
                lastHit = hit.collider.gameObject;
                if (activateKey.action != null && activateKey.action.WasPressedThisFrame())
                {
                    lastHit.Destroy();
                    lastHit = null;
                }
            }
            DrawBeam();
        }

        private void DrawBeam()
        {
            if (lastHit != null)
            {
                lineRenderer.startWidth = lineRenderer.endWidth = lineWidthHighlighted;
            }
            else
            {
                lineRenderer.startWidth = lineRenderer.endWidth = prevLineWidth;
            }
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, distance ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = distance;
        }
    }
}

//#endif