//#if WANDER_VR

using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZTraceHeight: MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public InputActionProperty activateKey;
        public GameObject placeHeightPrefab;
        public Material validBeam;
        public Material invalidBeam;

        [Header("LIDARQuery")]
        public int numPointsToSkip = 100;
        public float radius = 0.2f;
        public float distance = 100;

        [Header("Debug")]
        public bool useRays = false;

        LineRenderer lineRenderer;
        TextMeshPro activeHeightLabel;

        // Query
        TraceLineQuery query;
        Vector3 foundPoint;
        bool lastValid;

        private void Awake()
        {
            if (loader == null )
                loader = FindObjectOfType<LAZLoader>();
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
        }

        private void OnDisable()
        {
            if (activeHeightLabel != null)
                activeHeightLabel.transform.parent.Destroy();
            query = null;
            lastValid = false;
        }

        void Update()
        {
            // Get new query if not started.
            if (query == null)
            {
                Vector3 from = transform.position + transform.forward*lineRenderer.GetPosition(0).z;
                query = loader.StartLineTrace( from, transform.forward, numPointsToSkip, radius, distance );
            }
            else if (query != null && query.done) // If query is done.
            {
                if (activeHeightLabel == null)
                {
                    GameObject heightLabel = Instantiate( placeHeightPrefab );
                    activeHeightLabel = heightLabel.transform.GetChild( 0 ).GetComponent<TextMeshPro>();
                }
                activeHeightLabel.transform.parent.gameObject.SetActive( false );

                //      UnityEngine.Debug.Log( "Query took: " + sw.ElapsedMilliseconds + "ms was hit: " + (hit?"yes":"no") );
                lastValid = query.hit;
                if ( query.hit )
                {
                    foundPoint = query.vertex.position;
                    if (Physics.Raycast( foundPoint, Vector3.down, out RaycastHit hit, 1000, LayerMask.GetMask( "Default", "Ground" ) ))
                    {
                        // add box collider so that it can be removed with remove tool
                        activeHeightLabel.text = $"h={hit.distance.ToString( "0.0" )}m";
                        activeHeightLabel.transform.parent.gameObject.SetActive( true );
                        activeHeightLabel.transform.parent.position = foundPoint;
                        // set rotation
                        var f = Camera.main.transform.forward;
                        f.y = 0;
                        f.Normalize();
                        activeHeightLabel.transform.parent.forward  = f;
                    }
                }
                query = null;
            }

            if (activateKey.action!=null && activateKey.action.WasPressedThisFrame())
            {
                // Only if active label was activated (that is valid) and pinned, Null it so that a new will be spawned.
                if (activeHeightLabel != null && activeHeightLabel.transform.parent.gameObject.activeSelf)
                {
                    activeHeightLabel = null;
                }
            }
          
            DrawBeam();
        }

        private void DrawBeam()
        {
            float d;
            if (lastValid)
            {
                d = (transform.position - foundPoint).magnitude;
            }
            else
            {
                d = distance;
            }
            lineRenderer.sharedMaterial = lastValid ? validBeam : invalidBeam;
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, d ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = d;
            XRNetworkState.LaserColor[(int)XRHand.Left]  = validBeam ? validBeam.GetColor( "_Color" ) : invalidBeam.GetColor( "_Color" );
        }
    }
}

//#endif