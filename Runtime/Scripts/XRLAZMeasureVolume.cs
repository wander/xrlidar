//#if WANDER_VR

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;
using TMPro;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZMeasureVolume: MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public InputActionProperty placePointKey;
        public InputActionProperty confirmKey;
        public GameObject placeVolumePointPrefab;
        public GameObject volumeInfoPrefab;
        public Material validBeam;
        public Material invalidBeam;
        public Material volumeMaterial;

        [Header("LIDARQuery")]
        public int numPointsToSkip = 100;
        public float radius = 0.2f;
        public float distance = 100;

        [Header("Debug")]
        public bool useRays = false;

        LineRenderer lineRenderer;
        List<GameObject> points;
        GK.ConvexHullCalculator calculator;
        XRWarningMessage warningMsg;

        // Query
        TraceLineQuery query;
        Vector3 foundPoint;
        bool lastValid;
        bool placedSinceValid;


        private void Awake()
        {
            if (loader == null )
                loader = FindObjectOfType<LAZLoader>();
            warningMsg = FindObjectOfType<XRWarningMessage>( true );
            lineRenderer = GetComponent<LineRenderer>();
            points = new List<GameObject>();
            calculator = new GK.ConvexHullCalculator();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
        }

        private void OnDisable()
        {
            Cleanup();
        }

        void Cleanup()
        {
            points.ForEach( p => p.Destroy() );
            points.Clear();
            query = null;
            lastValid = false;
        }

        void Update()
        {
            // Start ray query if not yet started.
            if (query == null)
            {
                Vector3 from = transform.position + transform.forward*lineRenderer.GetPosition(0).z;
                query = loader.StartLineTrace( from, transform.forward, numPointsToSkip, radius, distance );
            }
            else if (query != null && query.done) // Handle the result of a previously started ray query.
            {
                lastValid  = query.hit;
                foundPoint = query.vertex.position;
                placedSinceValid = false;
                query = null;
            }

            // If place point key is pressed, place a point.
            if (placePointKey.action.WasPressedThisFrame() && lastValid && !placedSinceValid)
            {
                placedSinceValid = true;
                var point = Instantiate( placeVolumePointPrefab, foundPoint, Quaternion.identity );
                points.Add( point );
            }

            // If done with placing points and confirm is pressed.
            if (confirmKey.action.WasPressedThisFrame())
            {
                if (points.Count < 4)
                {
                    warningMsg.AddWarning( "A minimum of 4 points is required." );
                }
                else
                {
                    // Build convex hull geometry
                    List<Vector3> verts = new List<Vector3>();
                    List<int> tris = new List<int>();
                    List<Vector3> normals = new List<Vector3>();
                    calculator.GenerateHull( points.Select( p => p.transform.position ).ToList(), false, ref verts, ref tris, ref normals );
                    Mesh m = new Mesh();
                    m.SetVertices( verts );
                    m.SetTriangles( tris, 0 );
                    m.SetNormals( normals );
                    // Add geometry to game object
                    GameObject volume = new GameObject("Volume");
                    var meshRenderer = volume.AddComponent<MeshRenderer>();
                    var meshFilter   = volume.AddComponent<MeshFilter>();
                    meshRenderer.sharedMaterial = volumeMaterial;
                    meshFilter.sharedMesh = m;
                    // find closest point to camera on volume
                    float closestDist = float.PositiveInfinity;
                    int vClosestIdx = 0;
                    for ( int i = 0; i < verts.Count; i++ )
                    {
                        float projDist = Vector3.Dot( verts[i] - Camera.main.transform.position, Camera.main.transform.forward );
                        if ( projDist < closestDist )
                        {
                            vClosestIdx = i;
                            closestDist = projDist;
                        }
                    }
                    float volumeVal = CalcVolume( verts, tris, normals );
                    GameObject volumeInfo = Instantiate(volumeInfoPrefab);
                    //  volumeInfo.transform.parent = volume.transform;
                    volumeInfo.transform.position = verts[vClosestIdx];
                    volumeInfo.GetComponentInChildren<TextMeshPro>().text = $"v={volumeVal.ToString("0.0")}m3";
                    // set rotation
                    var f = Camera.main.transform.forward;
                    f.y = 0;
                    f.Normalize();
                    volumeInfo.transform.forward = f;
                    // cleanup
                    Cleanup();
                }
            }
            
            DrawBeam();
        }

        private void DrawBeam()
        {
            float d;
            if (lastValid)
            {
                d = (transform.position - foundPoint).magnitude;
            }
            else
            {
                d = distance;
            }
            lineRenderer.sharedMaterial = lastValid ? validBeam : invalidBeam;
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, d ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = d;
            XRNetworkState.LaserColor[(int)XRHand.Left]  = lastValid ? validBeam.GetColor( "_Color" ) : invalidBeam.GetColor( "_Color" );
        }

        float CalcVolume(List<Vector3> verts, List<int> tris, List<Vector3> normals)
        {
            float volume = 0;
            for ( int t = 0; t < tris.Count; t += 3)
            {
                var i0 = tris[t+0];
                var i1 = tris[t+1];
                var i2 = tris[t+2];
                Vector3 v0 = verts[i0];
                Vector3 v1 = verts[i1];
                Vector3 v2 = verts[i2];
                Vector3 cr = Vector3.Cross( (v1-v0), (v2-v0) );
                Vector3 n  = cr.normalized;
                float area = cr.magnitude * 0.5f;
                float h = -Vector3.Dot( verts[0] - v0, n );
                float v = (area * h)/3;
                volume += v;
            }
            return volume;
        }
    }
}

//#endif