//#if WANDER_VR

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Wander
{
    [RequireComponent( typeof( LineRenderer ) )]
    public class XRLAZAnnotate : MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public ActionBasedController controller;
        public InputActionProperty holdToAnnotate;
        public InputActionProperty nextClassification;
        public GameObject placeSpherePrefab;
        public Material [] classifierMats;
        public float IntensityWhenHit;

        [Header("LIDARVertexQuery")]
        public float distance = 1;
        public float distChangeLengthSpeed = 0.008f;
        public float sphereRadius = 0.05f;
        public float sphereChangeSizeSpeed = 0.008f;

        // Graphics
        LineRenderer lineRenderer;
        GameObject annotateSphere;
        TraceVerticesQuery verticesQuery;

        private void Awake()
        {
            if (loader == null)
                loader = FindObjectOfType<LAZLoader>();
            lineRenderer = GetComponent<LineRenderer>();
            controller = GetComponent<ActionBasedController>();
        }

        private void OnEnable()
        {
            annotateSphere = Instantiate( placeSpherePrefab );
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
        }

        private void OnDisable()
        {
            annotateSphere.Destroy();
            annotateSphere = null;
            verticesQuery = null;
        }

        void Update()
        {
            // Since applying annotation is done in diff thread, query this button on main thread first.
            bool applyAnnotate = holdToAnnotate.action.IsPressed();
       //     print( applyAnnotate );

            Vector3 from = transform.position + transform.forward*lineRenderer.GetPosition( 0 ).z;
            Vector3 to   = transform.position + transform.forward*distance;

            // Start vertex query
            if (verticesQuery == null && applyAnnotate)
            {
                verticesQuery = loader.StartVertexQuery( to, sphereRadius );
            }
            else if (verticesQuery != null && verticesQuery.done)
            {
                loader.StartGenericTask( () =>
                {
                    var vqSlice = verticesQuery;
                    if (vqSlice == null) // When OnDisable is called in mean time, this might have become null;
                        return;

                    HashSet<LAZOctreeCell> uniqueCells = new HashSet<LAZOctreeCell>();

                    // Now we have the indices to the vertices in the cell.
                    for (int i = 0;i < vqSlice.indices.Count;i++)
                    {
                        var renderer = vqSlice.indices[i].Item1;
                        var lazCell  = vqSlice.indices[i].Item2;
                        var vertexId = vqSlice.indices[i].Item3;

                        uniqueCells.Add( lazCell );

                        // TODO, one issue here: if we apply a change to the cell, it will apply for all renderers pointing to this data.
                        lazCell.ApplyChangeToVertices( ( List<LAZVertex> vertices ) =>
                        {
                            LAZVertex vertex = vertices[vertexId];
                            vertex.classification = (ushort)(XRLAZState.activeClassification<<8); // 65535 is scaled to [0-1]. So pre-multiply with 256.
                            vertices[vertexId] = vertex;
                        } );
                    }

                    foreach (var cell in uniqueCells)
                    {
                        if (cell.LeafReady)
                        {
                            cell.WriteLeafData();
                            cell.MarkDirty(); // This causes it to reload even though it may not have been unloaded by the loader due to culling.
                        }
                    }
                    verticesQuery = null;
                } );
            }

            UpdateInput();
            UpdateGraphics();
        }

        private void UpdateInput()
        {
            var joyInputY = controller.translateAnchorAction.action.ReadValue<Vector2>();
            var joyInputX = controller.rotateAnchorAction.action.ReadValue<Vector2>();

            if (Mathf.Abs( joyInputX.x ) > 0.5f)
            {
                sphereRadius += joyInputX.x*sphereChangeSizeSpeed;
                sphereRadius  = Mathf.Clamp( sphereRadius, 0.02f, 2f );
            }

            distance += joyInputY.y * distChangeLengthSpeed;
            distance  = Mathf.Clamp( distance, 0.1f, 20f );

            if (nextClassification.action.WasPressedThisFrame())
            {
                XRLAZState.activeClassification += 1;
                if (XRLAZState.activeClassification == classifierMats.Length)
                {
                    XRLAZState.activeClassification = 0;
                }
            }
        }

        private void UpdateGraphics()
        {
            float d = distance;
            lineRenderer.sharedMaterial = classifierMats[XRLAZState.activeClassification];
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, d ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = d;
            XRNetworkState.LaserColor[(int)XRHand.Left]  = lineRenderer.sharedMaterial.GetColor( "_Color" );
            // sphere
            annotateSphere.transform.position = transform.position + transform.forward*d;
            annotateSphere.transform.localScale = Vector3.one * sphereRadius * 2;
            annotateSphere.GetComponent<MeshRenderer>().sharedMaterial = lineRenderer.sharedMaterial;
        }
    }
}

//#endif