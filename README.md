## XRLidar (HDRP)

This framework builds upon XRFramework2 and adds lidar interaction
to XR.


### Dependencies (GIT Pacakages)
- xrframework2
- lidar
- utils


![Image](./sample.png)
